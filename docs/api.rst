pyteleinfo Python API
=====================

teleinfo.codec
--------------

.. automodule:: teleinfo.codec
   :members:

teleinfo.exceptions
-------------------

.. automodule:: teleinfo.exceptions
   :members:



