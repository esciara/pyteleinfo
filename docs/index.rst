Welcome to pyteleinfo's documentation!
======================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   development

.. toctree::
   :caption: Programmer Reference

   api

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
