"""
Package constants
"""

#: Teleinfo specification base encoding.
ENCODING = "ascii"
#: NULL BIT
NULL = "\x00"
#: Start of TeXt token.
STX = "\x02"
#: End of TeXt token.
ETX = "\x03"
#: SPace token.
SP = "\x20"
#: Horizontal Tab token.
# HT = "\x09"
HT = "\t"
#: End Of Transmission token.
EOT = "\x04"
#: Message chunk end token.
LF = "\n"
CR = "\r"

# Dict keys for info groups
LABEL = "label"
DATA = "data"
CHECKSUM = "checksum"

# #### UNITS OF MEASUREMENT ####
# Power units
UNIT_WATT = "W"
UNIT_VOLT_AMPERE = "VA"

# Energy units
UNIT_WATT_HOUR = "Wh"

# Intensity units
UNIT_AMPERE = "A"

# Time units
UNIT_MINUTES = "min"

# #### INFO GROUPS ####
INFO_GROUPS = {
    "ADCO": {
        "short_description": "Adresse du compteur",
        "description": "Adresse du compteur",
        "n_char": 12,
        "unit": None,
    },
    "OPTARIF": {
        "short_description": "Option tarifaire",
        "description": "Option tarifaire choisie",
        "n_char": 4,
        "unit": None,
    },
    "ISOUSC": {
        "short_description": "Intensité souscrite",
        "description": "Intensité souscrite",
        "n_char": 2,
        "unit": UNIT_AMPERE,
    },
    "BASE": {
        "short_description": "Index",
        "description": "Index option Base",
        "n_char": 9,
        "unit": UNIT_WATT_HOUR,
    },
    "HCHC": {
        "short_description": "Index Heures Creuses",
        "description": "Index option Heures Creuses - Heures Creuses",
        "n_char": 9,
        "unit": UNIT_WATT_HOUR,
    },
    "HCHP": {
        "short_description": "Index Heures Pleines",
        "description": "Index option Heures Creuses - Heures Pleines",
        "n_char": 9,
        "unit": UNIT_WATT_HOUR,
    },
    "EJPHN": {
        "short_description": "Index Heures Normales",
        "description": "Index option EJP - Heures Normales",
        "n_char": 9,
        "unit": UNIT_WATT_HOUR,
    },
    "EJPHPM": {
        "short_description": "Index Heures de Pointe Possible",
        "description": "Index option EJP - Heures de Pointe Possible",
        "n_char": 9,
        "unit": UNIT_WATT_HOUR,
    },
    "BBRHCJB": {
        "short_description": "Index Heures Creuses Jours Bleus",
        "description": "Index option Tempo - Heures Creuses Jours Bleus",
        "n_char": 9,
        "unit": UNIT_WATT_HOUR,
    },
    "BBRHPJB": {
        "short_description": "Index Heures Pleines Jours Bleus",
        "description": "Index option Tempo - Heures Pleines Jours Bleus",
        "n_char": 9,
        "unit": UNIT_WATT_HOUR,
    },
    "BBRHCJW": {
        "short_description": "Index Heures Creuses Jours Blancs",
        "description": "Index option Tempo - Heures Creuses Jours Blancs",
        "n_char": 9,
        "unit": UNIT_WATT_HOUR,
    },
    "BBRHPJW": {
        "short_description": "Index Heures Pleines Jours Blancs",
        "description": "Index option Tempo - Heures Pleines Jours Blancs",
        "n_char": 9,
        "unit": UNIT_WATT_HOUR,
    },
    "BBRHCJR": {
        "short_description": "Index Heures Creuses Jours Rouges",
        "description": "Index option Tempo - Heures Creuses Jours Rouges",
        "n_char": 9,
        "unit": UNIT_WATT_HOUR,
    },
    "BBRHPJR": {
        "short_description": "Index Heures Pleines Jours Rouges",
        "description": "Index option Tempo - Heures Pleines Jours Rouges",
        "n_char": 9,
        "unit": UNIT_WATT_HOUR,
    },
    "PEJP": {
        "short_description": "Préavis Début EJP (30 min)",
        "description": "Préavis Début EJP (30 min)",
        "n_char": 2,
        "unit": UNIT_MINUTES,
    },
    "PTEC": {
        "short_description": "Couleur du Jour",
        "description": "Période Tarifaire en cours",
        "n_char": 4,
        "unit": None,
    },
    "DEMAIN": {
        "short_description": "Couleur du lendemain",
        "description": "Couleur du lendemain",
        "n_char": 4,
        "unit": None,
    },
    "IINST": {
        "short_description": "Intensité Instantanée",
        "description": "Intensité Instantanée",
        "n_char": 3,
        "unit": UNIT_AMPERE,
    },
    "ADPS": {
        "short_description": "Dépassement De Puissance Souscrite",
        "description": "Avertissement de Dépassement De Puissance Souscrite",
        "n_char": 3,
        "unit": UNIT_AMPERE,
    },
    "IMAX": {
        "short_description": "Intensité maximale periode",
        "description": "Intensité maximale appelée",
        "n_char": 3,
        "unit": UNIT_AMPERE,
    },
    "PAPP": {
        "short_description": "Puissance apparente",
        "description": "Puissance apparente",
        "n_char": 5,
        "unit": UNIT_VOLT_AMPERE,
    },
    "HHPHC": {
        "short_description": "Horaire Heures Pleines/Creuses",
        "description": "Horaire Heures Pleines Heures Creuses",
        "n_char": 1,
        "unit": None,
    },
    "MOTDETAT": {
        "short_description": "Mot d'état du compteur",
        "description": "Mot d'état du compteur",
        "n_char": 6,
        "unit": None,
    },
}
