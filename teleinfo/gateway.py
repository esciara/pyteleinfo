import asyncio
import json
from asyncio import StreamReader, StreamWriter
from typing import Callable, List, Optional, Tuple, TypeVar

import serial
from loguru import logger
from serial.tools import list_ports, list_ports_common

from . import aioserial
from .codec import decode
from .const import ENCODING, ETX
from .exceptions import TeleinfoError

KNOWN_GATEWAYS = [
    {
        "manufacturer": "FTDI",
        "product": "FT230X Basic UART",
        "vid": 0x0403,
        "pid": 0x6015,
    }
]
T = TypeVar("T")


async def open_connection(
    port: str,
    listener: Optional[Callable[..., T]] = None,
    timeout: Optional[float] = None,
) -> Tuple[StreamReader, StreamWriter]:
    return await asyncio.wait_for(
        aioserial.open_teleinfo_serial_connection(  # type: ignore
            url=port, listener=listener
        ),
        timeout,
    )


async def read_frame(reader: StreamReader, timeout: Optional[float] = None):
    return await asyncio.wait_for(
        reader.readuntil(separator=ETX.encode(ENCODING)), timeout
    )


async def discover() -> List[list_ports_common.ListPortInfo]:
    gateways = await discover_known_gateways()
    gateways.extend(await scan_for_gateways())
    if len(gateways) > 0:
        logger.info(f"Known and scanned gateways found: {gateways}")
    else:
        logger.info(f"No known or scanned gateways found.")
    return gateways


async def discover_known_gateways() -> List[list_ports_common.ListPortInfo]:
    logger.info("Looking for known teleinfo USB Gateways...")
    logger.info(f"Known Gateways: {KNOWN_GATEWAYS}")
    found_gateways = []
    for gw in KNOWN_GATEWAYS:
        found_gateways.extend(
            [
                i
                for i in list_ports.comports()
                if i.vid == gw["vid"] and i.pid == gw["pid"]
            ]
        )
    n = len(found_gateways)
    if n == 0:
        logger.info(f"No known gateway found.")
    else:
        logger.info(f"Gateways found: '{[i.device for i in found_gateways]}' .")
        await log_ports_info(found_gateways)
    return found_gateways


async def scan_for_gateways() -> List[list_ports_common.ListPortInfo]:
    logger.info("Scanning serial ports...")
    ports_to_scan = [
        p
        for p in list_ports.comports()
        if {"vid": p.vid, "pid": p.pid} not in KNOWN_GATEWAYS
    ]
    logger.debug("Full info:")
    logger.info(f"List of ports to scan: {[i.device for i in ports_to_scan]}")
    await log_ports_info(ports_to_scan)
    gateways = []
    for p in ports_to_scan:
        gateway = await check_port_sends_teleinfo_data(p.device)
        if gateway:
            logger.info(
                f"Port {p.device} receives valid teleinfo frames! "
                f"Adding to list of gateways."
            )
            gateways.append(p)
    if len(gateways) == 0:
        logger.info("All com ports scanned. No port with teleinfo found.")
    return gateways


async def log_ports_info(ports):
    logger.debug("Ports full info:")
    logger.debug(
        "device, name, description, hwid, "
        "vid, pid, serial_number, location, "
        "manufacturer, product, interface"
    )
    for i in ports:
        logger.debug(
            f"{i.device}, {i.name}, {i.description}, {i.hwid}, "
            f"{i.vid}, {i.pid}, {i.serial_number}, {i.location}, "
            f"{i.manufacturer}, {i.product}, {i.interface}"
        )


async def check_port_sends_teleinfo_data(
    port, raw_flag=False, max_frames=3, timeout=5.0
) -> Optional[str]:
    gateway = None
    logger.info(
        f"Trying to read port '{port}' for {timeout} secs... "
        f"Will print a max of {max_frames} frames..."
    )
    try:
        reader, _ = await open_connection(port, timeout=timeout)
        await read_frame(reader, timeout)
        for _ in range(max_frames):
            logger.debug(
                await extract_frame_to_print(reader, raw_flag, timeout=timeout)
            )
    except serial.SerialException as e:
        logger.debug(f"<error>{e.__repr__()}</>")
    except TeleinfoError as e:
        logger.debug(f"<error>{e.__repr__()}</>")
    except asyncio.TimeoutError:
        logger.info("Timeout!")
    else:
        gateway = port
    # Add a sleep so that output buffer can be flushed
    await asyncio.sleep(0)
    return gateway


async def extract_frame_to_print(reader, raw_flag, timeout):
    frame_to_print = await read_frame(reader, timeout=timeout)
    if raw_flag:
        frame_to_print = f"{frame_to_print}"
    else:
        frame_to_print = json.dumps(decode(frame_to_print))
    return frame_to_print
