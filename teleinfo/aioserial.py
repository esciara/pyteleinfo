import asyncio
from asyncio import StreamReaderProtocol

import serial
from bitstring import Bits, ConstBitStream, ReadError
from loguru import logger
from serial_asyncio import SerialTransport

from .const import ENCODING, ETX

SERIAL_EVENT_CONNECTION_MADE = "connection_made"
SERIAL_EVENT_CONNECTION_LOST = "connection_lost"
SERIAL_EVENT_FRAME_RECEIVED = "data_received"
TELEINFO_SERIAL_CONNECTION_KWARGS = {
    "baudrate": 1200,
    "bytesize": serial.SEVENBITS,
    "parity": serial.PARITY_EVEN,
    "stopbits": serial.STOPBITS_ONE,
    "rtscts": 1,
}


class TeleinfoStreamReaderProtocol(StreamReaderProtocol):
    """Adds callback handling to StreamReaderProtocol helper class.
    """

    def __init__(
        self, stream_reader, client_connected_cb=None, loop=None, listener=None
    ):
        super().__init__(stream_reader, client_connected_cb, loop)
        self._listener = listener
        self._transport = None
        self._dirty_first_frame_skipped = None

    def send_event_to_listener(self, event):
        if self._listener:
            self._transport.loop.create_task(self._listener(event))

    def connection_made(self, transport):
        self._transport = transport
        self._dirty_first_frame_skipped = False
        self.send_event_to_listener(SERIAL_EVENT_CONNECTION_MADE)
        super().connection_made(transport)

    def connection_lost(self, exc):
        self.send_event_to_listener(SERIAL_EVENT_CONNECTION_LOST)
        super().connection_lost(exc)

    def data_received(self, data):
        if not self._dirty_first_frame_skipped:
            bits = ConstBitStream(data)
            try:
                bits.readto(ETX.encode(ENCODING), bytealigned=True)
            except ReadError:
                logger.info(
                    f"Skipping first frame: End of TeXt "
                    f"({ETX.encode(ENCODING)}) not found in data: "
                    f"({data}). Discarding whole received data"
                )
                return
            else:
                logger.info(
                    "Skipping first frame: discarding up to "
                    f"End of TeXt ({ETX.encode(ENCODING)})."
                )
                data = bits[bits.pos :].bytes  # noqa: E203
                self._dirty_first_frame_skipped = True

        end_of_frames_found = Bits(data).findall(ETX.encode(ENCODING), bytealigned=True)
        if end_of_frames_found:
            for _ in end_of_frames_found:
                self.send_event_to_listener(SERIAL_EVENT_FRAME_RECEIVED)
        super().data_received(data)


async def create_serial_connection(loop, protocol_factory, **kwargs):
    # Make sure that timeouts are set to 0 at creation time as
    # they will crash with termios.error: (22, 'Invalid argument')
    # on Debian (maybe other platforms) if timeout is set
    # once the serial is open
    # TODO open an issue on github on this and refer to it here
    kwargs.update({"timeout": 0, "write_timeout": 0})
    ser = serial.serial_for_url(**kwargs)
    protocol = protocol_factory()
    transport = SerialTransport(loop, protocol, ser)
    return transport, protocol


async def open_teleinfo_serial_connection(
    *, loop=None, limit=asyncio.streams._DEFAULT_LIMIT, url="", listener=None
):
    """A wrapper for create_serial_connection() returning a (reader,
    writer) pair with a protocol specific for teleinfo serial connection.

    The reader returned is a StreamReader instance; the writer is a
    StreamWriter instance.

    The arguments are all the usual arguments to Serial(). Additional
    optional keyword arguments are loop (to set the event loop instance
    to use) and limit (to set the buffer limit passed to the
    StreamReader.

    This function is a coroutine.
    """
    kwargs = {"url": url}
    kwargs.update(TELEINFO_SERIAL_CONNECTION_KWARGS)
    if loop is None:
        loop = asyncio.get_event_loop()
    reader = asyncio.StreamReader(limit=limit, loop=loop)
    protocol = TeleinfoStreamReaderProtocol(reader, loop=loop, listener=listener)
    transport, _ = await create_serial_connection(
        loop=loop, protocol_factory=lambda: protocol, **kwargs
    )
    writer = asyncio.StreamWriter(transport, protocol, reader, loop)
    return reader, writer


async def open_serial_connection(
    *, loop=None, limit=asyncio.streams._DEFAULT_LIMIT, **kwargs
):
    """A wrapper for create_serial_connection() returning a (reader,
    writer) pair.

    The reader returned is a StreamReader instance; the writer is a
    StreamWriter instance.

    The arguments are all the usual arguments to Serial(). Additional
    optional keyword arguments are loop (to set the event loop instance
    to use) and limit (to set the buffer limit passed to the
    StreamReader.

    This function is a coroutine.
    """
    if loop is None:
        loop = asyncio.get_event_loop()
    reader = asyncio.StreamReader(limit=limit, loop=loop)
    protocol = asyncio.StreamReaderProtocol(reader, loop=loop)
    transport, _ = await create_serial_connection(
        loop=loop, protocol_factory=lambda: protocol, **kwargs
    )
    writer = asyncio.StreamWriter(transport, protocol, reader, loop)
    return reader, writer
