from teleinfo.console.application import Application


def main():
    return Application().run()


if __name__ == "__main__":
    main()
