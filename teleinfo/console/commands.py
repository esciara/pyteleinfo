import asyncio

from cleo import Command
from loguru import logger

from ..gateway import (
    check_port_sends_teleinfo_data,
    discover_known_gateways,
    scan_for_gateways,
)


class BaseCommand(Command):
    """
    Teleinfo Base Command
    """


class PortCommand(BaseCommand):
    """
    Teleinfo port

    port
        {port : Port to listen from}
        {--r|raw : If set, bytes output for the port will be printed instead of
        decoded json}
    """

    def handle(self):
        loop = asyncio.get_event_loop()
        loop.run_until_complete(self.async_handle())

    async def async_handle(self):
        port = self.argument("port")
        await check_port_sends_teleinfo_data(port, self.option("raw"))
        # TODO find out how to return an proper exit code


class DiscoveryCommand(BaseCommand):
    """
    Teleinfo Discovery: will try to discover a port that receives teleinfo data

    discover
    """

    def handle(self):
        loop = asyncio.get_event_loop()
        loop.run_until_complete(self.async_handle())

    async def async_handle(self):
        try:
            gateways = await discover_known_gateways()
            if len(gateways) > 0:
                logger.info(
                    f"Known gateways found: {gateways}! Attempting reading data..."
                )
                for g in gateways:
                    await check_port_sends_teleinfo_data(g.device, timeout=5.0)
            await scan_for_gateways()
        except Exception as e:
            logger.error(f"Error : {e.__repr__()}")
